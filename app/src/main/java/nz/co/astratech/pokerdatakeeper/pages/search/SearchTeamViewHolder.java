package nz.co.astratech.pokerdatakeeper.pages.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import nz.co.astratech.pokerdatakeeper.R;

public class SearchTeamViewHolder extends RecyclerView.ViewHolder {
    Context context;
    ImageView btAddTeam;
    TextView tvName;
    TextView tvID;

    public SearchTeamViewHolder(View itemView) {
        super(itemView);
        this.context = itemView.getContext();

        btAddTeam = (ImageView) itemView.findViewById(R.id.image_button_add_team);
        tvName = (TextView) itemView.findViewById(R.id.textview_name);
        tvID = (TextView) itemView.findViewById(R.id.textview_id);
    }
}
