package nz.co.astratech.pokerdatakeeper.common;


/**
 * Created by laindow on 23/04/14.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;


/**
 * @class RITJsonHelper
 * @brief Provides functions for serialize/unserialize Objects into Json
 * Objects must implement IRITSerializable interface
 */

public class RITJsonHelper {
    private static Map<String, String> Percent_Encoding_Char;
    private static Gson mGsonTool = null;

    /**
     * Create and return Gson tool
     *
     * @return Gson object with predefined settings.
     */
    private static Gson getGsonTool() {

        Percent_Encoding_Char = new HashMap<String, String>();
        Percent_Encoding_Char.put("'", "%27");

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT, Modifier.PROTECTED);
//        builder.setExclusionStrategies(new ExclusionStrategy() {
//            @Override
//            public boolean shouldSkipField(FieldAttributes f) {
//                return f.getDeclaringClass().equals(RealmObject.class);
//            }
//
//            @Override
//            public boolean shouldSkipClass(Class<?> clazz) {
//                return false;
//            }
//        });
        builder.registerTypeAdapter(Date.class, new gsonUTCdateAdapter());
//        builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        builder.registerTypeAdapter(RealmString.class, new RealmStringTypeAdapter());
        builder.serializeNulls();
        builder.setPrettyPrinting();
        return builder.create();
    }

    public static Gson shared()
    {
        if (mGsonTool == null)
        {
            mGsonTool = getGsonTool();
        }
        return mGsonTool;
    }

    /**
     * Reset current Gson utility object
     *
     */
    public static void reset() {

        mGsonTool = null;
    }

    /**
     * Converts an Object to JSONArray.
     *
     * @param pObject object to serialize. Need to be IRITSerializable.
     * @return JSONArray created from input Object.
     */
    public static JSONArray generateJsonArray(IRITSerializable pObject)
    {
        //Check and create gson utility object if necessary
        if (mGsonTool == null )
        {
            mGsonTool = getGsonTool();
        }

        JsonArray jsonObj = mGsonTool.toJsonTree(pObject).getAsJsonArray();
        try
        {
            String encodedString = percentEncoding(jsonObj.toString());
            return new JSONArray(encodedString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Converts input object to the JSONObject. Input Object should implements the IRITSerializable interface.
     *
     * @param pObject An object to serialize should implements IRITSerializable interface.
     * @return JSONObject created from input object.
     */
    public static JSONObject toJson(IRITSerializable pObject)
    {
        if( pObject == null )
        {
            return null;
        }
        //Check and create gson utility object if necessary
        if (mGsonTool == null )
        {
            mGsonTool = getGsonTool();
        }

        String strVal = mGsonTool.toJson(pObject);

        try {
            String encodedString = percentEncoding( strVal );
            return new JSONObject(encodedString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Converts a provided String to the class type that is sent to that method
     *
     * @param pData  A JSON as a string
     * @param pType Class type of the type that should be return
     * @return IRITSerializable created from the input JSON of type clazz.
     */
    public static <T> T toObject(String pData, Type pType) {
        if( pData == null )
        {
            return null;
        }
        //Check and create gson utility object if necessary
        if (mGsonTool == null )
        {
            mGsonTool = getGsonTool();
        }
        return mGsonTool.fromJson(pData, pType);
    }

    /**
     * Performs encoding of the input String.
     *
     * @param pValue String to be encoded.
     * @return String after encoding.
     */
    public static String percentEncoding(String pValue) {
        Set<String> keys = Percent_Encoding_Char.keySet();
        for (String key : keys) {
            pValue = pValue.replaceAll(key, Percent_Encoding_Char.get(key));
        }
        return pValue;
    }

    /**
     * Converts input object to the String. Input Object should implements the IRITSerializable interface.
     *
     * @param pObject An object to serialize should implements IRITSerializable interface.
     * @return String created from input object.
     */
    public static String toString(IRITSerializable pObject)
    {
        if( pObject == null )
        {
            return null;
        }
        //Check and create gson utility object if necessary
        if (mGsonTool == null )
        {
            mGsonTool = getGsonTool();
        }

        return mGsonTool.toJson(pObject);
    }

    /**
     * Converts input object to the String. Input Object should implements the IRITSerializable interface.
     *
     * @param pObject An object to serialize should implements IRITSerializable interface.
     * @return String created from input object.
     */
    public static String toString(Object pObject, boolean isObject)
    {
        if( pObject == null )
        {
            return null;
        }
        //Check and create gson utility object if necessary
        if (mGsonTool == null )
        {
            mGsonTool = getGsonTool();
        }

        return mGsonTool.toJson(pObject);
    }

    /**
     * Converts input object to the String. Input Object should implements the IRITSerializable interface.
     *
     * @param pObject An object to serialize should implements IRITSerializable interface.
     * @return encoded String created from input object.
     */
    public static String toStringEnc(IRITSerializable pObject)
    {
        if( pObject == null )
        {
            return null;
        }
        //Check and create gson utility object if necessary
        if (mGsonTool == null )
        {
            mGsonTool = getGsonTool();
        }

        String strVal = mGsonTool.toJson(pObject);

        try {
            return percentEncoding(strVal);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static class gsonUTCdateAdapter implements JsonSerializer<Date>,JsonDeserializer<Date> {

        private final DateFormat dateFormat;
        private final DateFormat dateFormatShort;
        private final DateFormat dateFormatFixture;
        private final DateFormat dateFormatFood;

        public gsonUTCdateAdapter() {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());      //This is the format I need
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));                               //This is the key line which converts the date to UTC which cannot be accessed with the default serializer
            dateFormatShort = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            dateFormatShort.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateFormatFixture = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z", Locale.getDefault());
            dateFormatFixture.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateFormatFood = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.getDefault());
            dateFormatFood.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        @Override
        public synchronized JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(dateFormat.format(date));
        }

        @Override
        public synchronized Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
            try {
                Date resultValue = null;
                String dateString = jsonElement.getAsString();
                if( dateString != null )
                {
                    if (dateString.length() == 10)
                    {
                        resultValue = dateFormatShort.parse(dateString);
                    }
                    else if (dateString.contains(" +"))
                    {
                        try {
                            resultValue = dateFormatFixture.parse(dateString);
                        } catch (ParseException e) {
                            resultValue = dateFormatFood.parse(dateString);
                        }
                    }
                    else
                    {
                        resultValue = dateFormat.parse(dateString);
                    }
                }
                return resultValue;
            } catch (ParseException e) {
                throw new JsonParseException(e);
            }
        }
    }
}
