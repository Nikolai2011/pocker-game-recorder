package nz.co.astratech.pokerdatakeeper.pages.search;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.common.Constants;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;

public class FirebaseTeamSearch {
    private final String TAG = "FirebaseSearchQuery";

    // "~" can be replaced with "\uf8ff" (and vice versa) !!! Limit by "~" will exclude many non-ASCII symbols from search, e.g. Russian characters.
    private final String END_VALUE_IDENTIFIER = "\uf8ff";
    public final int QUERY_ITEMS_LIMIT = 3;

    public interface SearchStateChangedListener {
        void onSearchStateChanged(boolean isSearching);
    }

    public interface SearchResultsArrivedListener {
        void onSearchResultsArrived(List<TeamCredentialsModel> searchResults, boolean fromNextPage);
    }

    public interface SearchTypeChangedListener {
        void onSearchTypeChanged(String searchType);
    }

    private Context context;
    private SearchStateChangedListener searchStateListener;
    private SearchResultsArrivedListener searchResultsListener;
    private SearchTypeChangedListener searchTypeChangedListener;
    private Query searchQuery;

    private String searchType;
    private boolean isSearching = false;
    private List<TeamCredentialsModel> arraySearchResults;

//    public FirebaseTeamSearch(Context context) {
//        this(context, null);
//    }

    public FirebaseTeamSearch(Context context, String searchType) {
        this.context = context;
        this.searchType = searchType;

        searchQuery = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_NODE_TEAM_ID_NAME_MAPPING);
        arraySearchResults = new ArrayList<>();
    }

    public String convertToEndValue(String value) {
        return (value == null) ? null : value + END_VALUE_IDENTIFIER;
    }

    public String getSearchValueFromObj(TeamCredentialsModel obj) {
        if (obj == null) {
            throw new NullPointerException();
        }

        return (isSearchByID()) ? obj.getId() : obj.getName();
    }

    public void postQuery(boolean loadNextPage, String startID, String endID, String startName, String endName) {
        if (!validateSearching()) {
            return;
        }

        Query query = setupQuery(loadNextPage, startID, endID, startName, endName);
        startQuering(query, loadNextPage);
    }

    public void postQuery(boolean loadNextPage, String startValue, String endValue) {
        if (!validateSearching()) {
            return;
        }

        Query query = setupQuery(loadNextPage, startValue, endValue);
        startQuering(query, loadNextPage);
    }

    /**
     * Filtering by both name and ID (2 fields)
     */
    private Query setupQuery(boolean loadNextPage, String startID, String endID, String startName, String endName) {
        if (isSearchByID()) {
            throw new RuntimeException("Filtering with 2 fields is only allowed in searching by name.");
        }

        Query query = searchQuery;
        query = (isSearchByID()) ? query.orderByKey() : query.orderByValue();
        query = (loadNextPage) ? query.startAt(startName, startID) : query.startAt(startName); // Filter by both name and ID
        query = (loadNextPage) ? query.endAt(endName) : query.endAt(endName, endID);
        query = (loadNextPage) ? query.limitToFirst(QUERY_ITEMS_LIMIT) : query.limitToLast(QUERY_ITEMS_LIMIT);

        return query;
    }

    /**
     * Filtering by only one field: name or ID
     */
    private Query setupQuery(boolean loadNextPage, String startValue, String endValue) {
        if (startValue == null || endValue == null) {
            throw new NullPointerException();
        }

        Query query = searchQuery;
        query = (isSearchByID()) ? query.orderByKey() : query.orderByValue();
        query = query.startAt(startValue).endAt(endValue); // Filter by name or ID
        query = (loadNextPage) ? query.limitToFirst(QUERY_ITEMS_LIMIT) : query.limitToLast(QUERY_ITEMS_LIMIT);

        return query;
    }

    private void startQuering(Query query, final boolean isLoadNextPage) {
        if (query == null) {
            throw new NullPointerException("startQuering(): Query is null!");
        }

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                arraySearchResults.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    arraySearchResults.add(new TeamCredentialsModel(postSnapshot.getKey(), postSnapshot.getValue().toString()));
                }
                notifyResultsArrived(isLoadNextPage);
                setIsSearching(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadResults:onCancelled", databaseError.toException());

                setIsSearching(false);
            }
        });
    }

    private void notifyResultsArrived(boolean fromNextPage) {
        if (searchResultsListener != null) {
            searchResultsListener.onSearchResultsArrived(arraySearchResults, fromNextPage);
        }
    }

    private boolean validateSearching() {
        boolean isValid = true;

        if (isSearching()) {
            isValid = false;
            Toast.makeText(context, context.getString(R.string.search_already_running), Toast.LENGTH_SHORT).show();
        } else if (!isSearchByID() && !isSearchByName()) {
            isValid = false;
            Toast.makeText(context, context.getString(R.string.search_type_undefined), Toast.LENGTH_SHORT).show();
        } else {
            setIsSearching(true);
        }

        return isValid;
    }

    public boolean isSearching() {
        return isSearching;
    }

    private void setIsSearching(boolean isSearching) {
        if (this.isSearching != isSearching) {
            this.isSearching = isSearching;
            if (searchStateListener != null) {
                searchStateListener.onSearchStateChanged(this.isSearching);
            }
        }
    }

    public void setSearchStateChangedListener(SearchStateChangedListener listener) {
        this.searchStateListener = listener;
    }

    public void setSearchResultsArrivedListener(SearchResultsArrivedListener searchResultsListener) {
        this.searchResultsListener = searchResultsListener;
    }

    public void setSearchTypeChangedListener(SearchTypeChangedListener searchTypeChangedListener) {
        this.searchTypeChangedListener = searchTypeChangedListener;
        searchTypeChangedListener.onSearchTypeChanged(searchType);
    }

    public boolean isSearchByID() {
        return searchType.equals(context.getString(R.string.search_by_id));
    }

    public boolean isSearchByName() {
        return searchType.equals(context.getString(R.string.search_by_name));
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        if (this.searchType == null || !this.searchType.equals(searchType)) {
            this.searchType = searchType;
            if (searchTypeChangedListener != null) {
                searchTypeChangedListener.onSearchTypeChanged(this.searchType);
            }
        }
    }
}
