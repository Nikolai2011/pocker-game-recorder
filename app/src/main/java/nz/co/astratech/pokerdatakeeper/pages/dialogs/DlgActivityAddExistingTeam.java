package nz.co.astratech.pokerdatakeeper.pages.dialogs;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import nz.co.astratech.pokerdatakeeper.common.Constants;
import nz.co.astratech.pokerdatakeeper.common.TeamValidation;
import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.pages.main.BaseActivity;

public class DlgActivityAddExistingTeam extends BaseActivity {
    private final String TAG = "DialogAddTeam";

    DatabaseReference firebaseTeams;
    ValueEventListener teamDetector;

    TextView textView_hint;
    EditText editText_team_id;
    Button bt_add;
    Button bt_cancel;

    TeamValidation teamDataTeamValidation;

    private String selectedTeamID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);//Before SetContentView()
        setContentView(R.layout.activity_add_team);

        teamDataTeamValidation = new TeamValidation(this);

        textView_hint = (TextView) findViewById(R.id.textview_hint);
        bt_add = (Button) findViewById(R.id.bt_add_team);
        editText_team_id = (EditText) findViewById(R.id.edittext_team_id);

        bt_cancel = (Button) findViewById(R.id.bt_cancel);
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancelClicked();
            }
        });

        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleAddClicked();
            }
        });

        teamDetector = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    //Team doesn't exist

                    textView_hint.setText(R.string.team_id_does_not_exist_feedback);
                } else {
                    //Team exists! Adding...

                    String teamName = dataSnapshot.getValue().toString();

                    Intent intentResult = new Intent();

                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_BUNDLE_TEAM_ID, selectedTeamID);
                    bundle.putString(Constants.KEY_BUNDLE_TEAMNAME, teamName);
                    intentResult.putExtras(bundle);

                    setResult(RESULT_OK, intentResult);
                    finish();
                }

                bt_add.setEnabled(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "Firebase read operation, onCancelled: " + databaseError.toString());
                textView_hint.setText(String.format(getString(R.string.error), ": " + databaseError.getMessage()));
                bt_add.setEnabled(true);
            }
        };
    }

    private void handleAddClicked() {
        try {
            selectedTeamID = editText_team_id.getText().toString().trim();
            if (!validateData()){
                return;
            }

            bt_add.setEnabled(false);

            firebaseTeams = FirebaseDatabase.getInstance().getReference().child(Constants.FIREBASE_NODE_TEAM_ID_NAME_MAPPING + "/" + selectedTeamID);
            firebaseTeams.addListenerForSingleValueEvent(teamDetector);
        } catch (Exception ex) {
            Log.e(TAG, "handleAddClicked(): " + ex.getMessage());
            textView_hint.setText(String.format(getString(R.string.error), ": " + ex.getMessage()));
            bt_add.setEnabled(true);
        }
    }

    private void handleCancelClicked() {
        Intent intentResult = new Intent();
        setResult(RESULT_CANCELED, intentResult);
        finish();
    }

    private boolean validateData() {
        boolean valid = true;

        String validationHint = teamDataTeamValidation.validateTeamID(selectedTeamID, true);
        if (validationHint != null) {
            valid = false;

            if (textView_hint.getText().toString().equals(validationHint)){
                textView_hint.setTypeface(null, Typeface.BOLD);
            } else{
                textView_hint.setTypeface(null, Typeface.NORMAL);
            }
            textView_hint.setText(validationHint);
        }
        return valid;
    }
}
