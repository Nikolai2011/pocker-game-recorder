package nz.co.astratech.pokerdatakeeper.pages.main;

import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;

public class TeamAdapter extends RecyclerView.Adapter<TeamViewHolder> {
    public interface OnRemoveTeamClickedListener {
        void onRemoveTeamClicked(View v);
    }

    public interface OnElementClickListener {
        void onElementClicked(View v);

        void OnElementTouched(View v, MotionEvent event);
    }

    private ArrayList<TeamCredentialsModel> arrayTeams;
    private ArrayList<TeamCredentialsModel> arrayRecentlyAddedTeams; //Correctly resolve animation when views are recycled.
    private Comparator<TeamCredentialsModel> sortingRules;

    private TeamAdapter.OnElementClickListener listenerElementClick;
    private TeamAdapter.OnRemoveTeamClickedListener listenerRemoveTeam;
    private View.OnClickListener removeTeamOnClickHandler; //One onClick definition to rule them all
    private View.OnClickListener elementOnClickHandler; //One onClick definition to rule them all
    private View.OnTouchListener elementOnTouchHandler;

    public TeamAdapter(ArrayList<TeamCredentialsModel> teams) {
        arrayTeams = teams;

        arrayRecentlyAddedTeams = new ArrayList<>();
        removeTeamOnClickHandler = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listenerRemoveTeam != null) {
                    listenerRemoveTeam.onRemoveTeamClicked(v);
                }
            }
        };

        elementOnClickHandler = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listenerElementClick != null) {
                    listenerElementClick.onElementClicked(v);
                }
            }
        };

        elementOnTouchHandler = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (listenerElementClick != null) {
                    listenerElementClick.OnElementTouched(v, event);
                }
                return false;
            }
        };

        sortingRules = new Comparator<TeamCredentialsModel>() {
            @Override
            public int compare(TeamCredentialsModel o1, TeamCredentialsModel o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        Collections.sort(arrayTeams, sortingRules);
    }

    @Override
    public TeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_local_team, parent, false);
        return new TeamViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TeamViewHolder holder, int position) {
        TeamCredentialsModel team = arrayTeams.get(position);

        holder.elementWrapper.setTag(R.id.online_team, team);
        holder.elementWrapper.setOnClickListener(elementOnClickHandler);
        holder.elementWrapper.setOnTouchListener(elementOnTouchHandler);

        ReusableFunctions.setTintedImageDrawable(holder.context, holder.btRemoveTeam, R.drawable.svg_cross_circled, R.color.selector_remove_team);
        ReusableFunctions.setTintOnBackground(holder.context, holder.btRemoveTeam, R.color.selector_pressable_view_tint);
        holder.btRemoveTeam.setTag(R.id.online_team, team);
        holder.btRemoveTeam.setOnClickListener(removeTeamOnClickHandler);
        if (arrayRecentlyAddedTeams.contains(team)) {
            // Prevents animation from going to next view when original view is removed
            arrayRecentlyAddedTeams.remove(team);
            onAttachedAnimation(holder.btRemoveTeam);
        }

        if (team == null) {
            holder.tvName.setText(holder.context.getString(R.string.unresolved_team));
        } else {
            String name = team.getName();
            if (name != null) {
                holder.tvName.setText(name);
            }
            holder.tvID.setText(String.format(holder.context.getString(R.string.team_id_entry), team.getId()));
        }
    }

    @Override
    public int getItemCount() {
        if (arrayTeams != null) {
            return arrayTeams.size();
        }
        return 0;
    }

    public void onAttachedAnimation(View btRemove) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(btRemove, "rotation", 0f, 720f);
        animator.setDuration(2000);
        animator.setRepeatCount(0);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.start();
    }

    public void setOnRemoveTeamClickedListener(TeamAdapter.OnRemoveTeamClickedListener listener) {
        this.listenerRemoveTeam = listener;
    }

    public void setOnElementClickListener(OnElementClickListener listenerElementClick) {
        this.listenerElementClick = listenerElementClick;
    }

    public void removeTeam(TeamCredentialsModel team) {
        int index = arrayTeams.indexOf(team);
        arrayTeams.remove(team);
        this.notifyItemRemoved(index);
    }

    public void addTeam(TeamCredentialsModel team) {
        arrayTeams.add(team); //Adding
        Collections.sort(arrayTeams, sortingRules); //Sorting
        int index = arrayTeams.indexOf(team); //Finding index
        this.notifyItemInserted(index); //Then notifying where the newly added item is located now

        if (!arrayRecentlyAddedTeams.contains(team)) {
            arrayRecentlyAddedTeams.add(team);
        }
    }
}
