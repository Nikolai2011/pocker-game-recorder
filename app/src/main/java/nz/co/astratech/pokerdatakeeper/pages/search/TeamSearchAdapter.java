package nz.co.astratech.pokerdatakeeper.pages.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;

public class TeamSearchAdapter extends RecyclerView.Adapter<SearchTeamViewHolder> {
    public interface OnAddTeamClickedListener {
        void onAddTeamClicked(View v);
    }

    private List<TeamCredentialsModel> arrayTeams;
    private TeamSearchAdapter.OnAddTeamClickedListener listenerAddTeam;
    private View.OnClickListener add_team_click_handler;

    public TeamSearchAdapter(List<TeamCredentialsModel> displayedTeams) {
        this.arrayTeams = displayedTeams;

        add_team_click_handler = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listenerAddTeam != null) {
                    listenerAddTeam.onAddTeamClicked(v);
                }
            }
        };
    }

    @Override
    public SearchTeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_online_team, parent, false);
        return new SearchTeamViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SearchTeamViewHolder holder, int position) {
        TeamCredentialsModel team = arrayTeams.get(position);

        ReusableFunctions.setTintedImageDrawable(holder.context, holder.btAddTeam, R.drawable.svg_plus_circled, R.color.selector_add_team_tint);

        holder.btAddTeam.setOnClickListener(add_team_click_handler);
        holder.btAddTeam.setTag(R.id.online_team, team);

        String name = team.getName();
        if (name != null) {
            holder.tvName.setText(name);
        }
        holder.tvID.setText(String.format(holder.context.getString(R.string.team_id_entry), team.getId()));
    }

    @Override
    public int getItemCount() {
        if (arrayTeams != null) {
            return arrayTeams.size();
        }
        return 0;
    }

    public void setListenerAddTeamClicked(TeamSearchAdapter.OnAddTeamClickedListener listenerAddTeam) {
        this.listenerAddTeam = listenerAddTeam;
    }
}
