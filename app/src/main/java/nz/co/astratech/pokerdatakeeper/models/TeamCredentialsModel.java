package nz.co.astratech.pokerdatakeeper.models;

import com.google.gson.annotations.SerializedName;

public class TeamCredentialsModel {
    @SerializedName("teamID") private String id;
    @SerializedName("teamName") private String name;
    @SerializedName("isPasswordRequired") private boolean isPasswordRequired = true;

    public TeamCredentialsModel(){

    }

    public TeamCredentialsModel(String id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TeamCredentialsModel){
            String id = ((TeamCredentialsModel) obj).getId();
            return this.getId().equals(id);
        }

        return false;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPasswordRequired() {
        return isPasswordRequired;
    }

    public void setPasswordRequired(boolean passwordRequired) {
        isPasswordRequired = passwordRequired;
    }
}
