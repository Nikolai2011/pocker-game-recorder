package nz.co.astratech.pokerdatakeeper.reusable;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.TintableBackgroundView;
import android.support.v4.view.ViewCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.common.ThemeSupport;
import nz.co.astratech.pokerdatakeeper.pages.main.ThemeActivity;

public class ReusableFunctions {
    public static boolean isConnectedToNetwork(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static void setBackground(View view, Drawable background) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(background);
        } else {
            view.setBackgroundDrawable(background);
        }
    }

    public static void setTintOnBackground(Context context, View view, int colorStateListID) {
        ColorStateList colorStateList = getColorStateList(context, colorStateListID);

        if (view instanceof TintableBackgroundView) {
            ViewCompat.setBackgroundTintList(view, colorStateList);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public static void setTintedImageDrawable(Context context, ImageView imageView, int drawableID, int colorStateListID) {
        Drawable tintedDrawable = ReusableFunctions.getTintedDrawable(context, drawableID, colorStateListID);
        imageView.setImageDrawable(tintedDrawable);
    }

    public static void setTintedForeground(Context context, FrameLayout frameLayout, int drawableID, int colorStateListID) {
        Drawable tintedDrawable = ReusableFunctions.getTintedDrawable(context, drawableID, colorStateListID);
        frameLayout.setForeground(tintedDrawable);
    }

    public static void setTintedBackground(Context context, View view, int drawableID, int colorStateListID) {
        Drawable tintedDrawable = ReusableFunctions.getTintedDrawable(context, drawableID, colorStateListID);
        ReusableFunctions.setBackground(view, tintedDrawable);
    }

    private static ColorStateList getColorStateList(Context context, int selectorID) {
        return AppCompatResources.getColorStateList(context, selectorID);
    }

    private static Drawable getDrawable(Context context, int drawableID) {
        return AppCompatResources.getDrawable(context, drawableID);
    }

    private static Drawable getTintedDrawable(Context context, int drawableID, int colorStateListID) {
        Drawable drawable = ReusableFunctions.getDrawable(context, drawableID);
        ColorStateList colorStateList = ReusableFunctions.getColorStateList(context, colorStateListID);

        Drawable tintedDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTintList(tintedDrawable.mutate(), colorStateList);
        return tintedDrawable;
    }

    /**
     * Use this if HEIGHT depends on layout calculations and WIDTH must be equal to height
     */
    public static void setSizeBasedOnHeight(View v, ViewGroup.LayoutParams params) {
        params.width = v.getHeight();
        v.setLayoutParams(params);
    }

    /**
     * Use this if WIDTH depends on layout calculations and HEIGHT must be equal to height
     */
    public static void setSizeBasedOnWidth(View v, ViewGroup.LayoutParams params) {
        params.height = v.getWidth();
        v.setLayoutParams(params);
    }

//    private boolean haveNetworkConnection(Context context) {
//        boolean haveConnectedWifi = false;
//        boolean haveConnectedMobile = false;
//
//        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
//        for (NetworkInfo ni : netInfo) {
//            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
//                if (ni.isConnected())
//                    haveConnectedWifi = true;
//            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
//                if (ni.isConnected())
//                    haveConnectedMobile = true;
//        }
//        return haveConnectedWifi || haveConnectedMobile;
//    }
}
