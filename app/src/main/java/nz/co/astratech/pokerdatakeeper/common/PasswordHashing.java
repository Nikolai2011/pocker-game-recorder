package nz.co.astratech.pokerdatakeeper.common;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordHashing {

    public static String hashFromPassword(String password) {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
            return null;
        }

        Charset charsetEncoding = Charset.forName("UTF-8");
        byte[] passwordBytes = password.getBytes(charsetEncoding);
        md.update(passwordBytes);
        byte[] digestedBytes = md.digest();
        return new String(digestedBytes, charsetEncoding);
    }
}
