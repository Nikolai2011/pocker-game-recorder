package nz.co.astratech.pokerdatakeeper.pages.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.common.ThemeSupport;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;

public class BaseActivity extends ThemeActivity {
    View toolbarApp; //May be null
    ImageView imageview_settings; //May be null

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        prepareAppBarIfExists();
        if (imageview_settings != null) {
            imageview_settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ThemeSupport.getCurrentGlobalTheme() == ThemeSupport.THEME_BLUE_GRAY) {
                        ThemeSupport.setGlobalTheme(self, ThemeSupport.THEME_TEAL);
                    } else if (ThemeSupport.getCurrentGlobalTheme() == ThemeSupport.THEME_TEAL) {
                        ThemeSupport.setGlobalTheme(self, ThemeSupport.THEME_ORANGE);
                    } else if (ThemeSupport.getCurrentGlobalTheme() == ThemeSupport.THEME_ORANGE) {
                        ThemeSupport.setGlobalTheme(self, ThemeSupport.THEME_DEEP_PURPLE);
                    } else {
                        ThemeSupport.setGlobalTheme(self, ThemeSupport.THEME_BLUE_GRAY);
                    }
                }
            });
        }
    }

    private void prepareAppBarIfExists() {
        toolbarApp = findViewById(R.id.toolbar);
        if (toolbarApp != null) {
            TextView textviewTitle = (TextView) toolbarApp.findViewById(R.id.textview_activity_title);
            if (textviewTitle != null) {
                textviewTitle.setText(getActivityTitle());
            }

            imageview_settings = (ImageView) toolbarApp.findViewById(R.id.bt_settings);
            ReusableFunctions.setTintedImageDrawable(this, imageview_settings, R.drawable.svg_settings, R.color.selector_pressable_button_tint );
            ReusableFunctions.setTintOnBackground(this, imageview_settings, R.color.selector_pressable_view_tint);
        }
    }

    protected String getActivityTitle() {
        return getString(R.string.app_name);
    }
}
