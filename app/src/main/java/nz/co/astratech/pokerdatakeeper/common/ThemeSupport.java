package nz.co.astratech.pokerdatakeeper.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import nz.co.astratech.pokerdatakeeper.MobileApp;
import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.pages.main.ThemeActivity;

public class ThemeSupport {
    public interface IActivityTheme {
        String getLocalThemeName();

        void setLocalThemeName(String theme);
    }

    //    Theme identifiers. These must match those of color resource names (their postfixes).
    public final static String THEME_UNDEFINED = "ThemeUndefined";
    public final static String THEME_BLUE_GRAY = "ThemeBlueGray";
    public final static String THEME_TEAL = "ThemeTeal";
    public final static String THEME_ORANGE = "ThemeOrange";
    public final static String THEME_DEEP_PURPLE = "ThemeDeepPurple";

    public final static String[] THEME_NAMES = new String[]{THEME_BLUE_GRAY, THEME_TEAL, THEME_ORANGE, THEME_DEEP_PURPLE};

    private static String currentGlobalTheme = THEME_BLUE_GRAY;

    private ThemeActivity themeActivity;

    public ThemeSupport(ThemeActivity themeActivity) {
        if (themeActivity == null) {
            throw new NullPointerException();
        }

        this.themeActivity = themeActivity;
    }

    /**
     * @param activity    Activity that requested global theme change must apply this theme itself through recreation.
     *                    Only ThemeActivity applies themes on recreation. Other activities are not recreated
     * @param globalTheme
     */
    public static void setGlobalTheme(Activity activity, String globalTheme) {
        currentGlobalTheme = globalTheme;
//        activity.finish();
//        activity.startActivity(new Intent(activity, activity.getClass()));
//        activity.overridePendingTransition(android.R.anim.fade_in,
//                android.R.anim.fade_out);
        if (activity instanceof ThemeActivity) {
            activity.recreate();
        }
    }

    public static String getCurrentGlobalTheme() {
        return currentGlobalTheme;
    }


    /**
     * Use this to ALWAYS update activity theme BEFORE setContentView.
     * If you avoid updating a single time - only properties from default theme will be applied.
     * Checking for current activity theme and matching it against current global theme is unnecessary.
     */
    public void updateActivityTheme() {
        switch (currentGlobalTheme) {
            default:
            case THEME_BLUE_GRAY:
                themeActivity.getTheme().applyStyle(R.style.ThemeBlueGray, true);
                //activity.setTheme(R.style.ThemeBlueGray);
                break;
            case THEME_TEAL:
                themeActivity.getTheme().applyStyle(R.style.ThemeTeal, true);
                break;
            case THEME_ORANGE:
                themeActivity.getTheme().applyStyle(R.style.ThemeOrange, true);
                break;
            case THEME_DEEP_PURPLE:
                themeActivity.getTheme().applyStyle(R.style.ThemeDeepPurple, true);
                break;
        }

//        activity.getTheme().applyStyle(R.style.ThemeUpdate, true);
        themeActivity.setLocalThemeName(currentGlobalTheme);
        //this.reloadDrawableResources(themeActivity.getResources());
    }

//    public ColorStateList getColorSelectorResource(int selectorID) {
//        //Use this to get color selector and apply it to stateful tint of a view.
//        //If you don't do it in code but rather in app:backgroundTint, this method is not necessary.
//
//        //This Color Selector resolution is necessary to force android resource system to find
//        //values for colors used in each color selector passed here. This color resource lookup will
//        //be intercepted in ResWrapper class and rerouted to appropriate Theme color resource.
//
//        ColorStateList selector = null;
//        try {
//            Resources res = themeActivity.getResources();
//            XmlResourceParser parser = res.getXml(selectorID);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
////                selector = ColorStateList.createFromXml(res, parser, null); //Not working on API 23
//                selector = res.getColorStateList(selectorID, null);
//            } else {
//                selector = ColorStateList.createFromXml(res, parser);
//            }
//            parser.close();
//        } catch (Exception ex) {
//            Log.e("loadColorSelector", " " + ex.getMessage());
//        }
//        return selector;
//    }

//    private void reloadDrawableResources(Resources res) {
//        Log.e("Reloading Started", " ");
//
////        for (int i = 0; i < FILE_COLOR_IDS_TO_UPDATE.length; i++) {
////            try {
////                res.getColor(i);
////            } catch (Exception ex) {
////                Log.e("reloadDrawableResources", "Color. " + res.getResourceEntryName(FILE_COLOR_IDS_TO_UPDATE[i]) + ". " + ex.getMessage());
////            }
////        }
////
////        for (int i = 0; i < FILE_COLOR_SELECTOR_IDS_TO_UPDATE.length; i++) {
////            try {
////                XmlResourceParser parser = res.getXml(FILE_COLOR_SELECTOR_IDS_TO_UPDATE[i]);
////                ColorStateList.createFromXml(res, parser);
////                parser.close();
////            } catch (Exception ex) {
////                Log.e("reloadDrawableResources", "ColorStateList. " + res.getResourceEntryName(FILE_COLOR_SELECTOR_IDS_TO_UPDATE[i]) + ". " + ex.getMessage());
////            }
////        }
//
////        for (int i = 0; i < FILE_RESOURCE_IDS_TO_UPDATE.length; i++) {
////            try {
////                XmlResourceParser parser = res.getXml(FILE_RESOURCE_IDS_TO_UPDATE[i]);
////                Drawable.createFromXml(res, parser); //Reinitializes cache that prevented resource colors to be dynamically resolved!
////                parser.close();
////            } catch (Exception ex) {
////                Log.e("reloadDrawableResources", "Drawable. " + res.getResourceEntryName(FILE_RESOURCE_IDS_TO_UPDATE[i]) + ". " + ex.getMessage());
////            }
////        }
//
//        Log.e("Reloading Finished", " ");
//    }

//    private static int resolveToCustomThemeColorID(Resources res, int requestedResourceId, String themeName) {
//        //Color Selector Resource IDs can be passed to this method.
//
//        String fullyQuilifiedName = res.getResourceName(requestedResourceId);
//        String resourceName = res.getResourceEntryName(requestedResourceId);
//        if (resourceName.startsWith(DYNAMIC_COLOR_PREFIX)) {
//            fullyQuilifiedName = fullyQuilifiedName.replace(DYNAMIC_COLOR_PREFIX, "");
//        }
//
//        String themeColorCheck = fullyQuilifiedName + themeName;
//        int resolvedID = res.getIdentifier(themeColorCheck, null, null); //0 if no resource has name that matches "themeColorCheck"
//        if (resolvedID != 0) {
//            Log.e("Resolved to ", themeColorCheck);
//        } else {
//            Log.e("CANNOT resolve to ", themeColorCheck);
//        }
//        return resolvedID;
//    }


//    public static class ResWrapper extends Resources {
//        private final String TAG = "ResWrapper";
//        private Context context;
//        private IActivityTheme themeActivity;
//        XmlResourceParser parser;
//
//        public ResWrapper(Resources base, IActivityTheme themeActivity) {
//            super(base.getAssets(), base.getDisplayMetrics(), base.getConfiguration());
//            this.context = MobileApp.getStaticContext();
//            this.themeActivity = themeActivity;
//
//            Log.e(TAG, "ResWrapper constructor has been called.");
//        }
//
//        @Override
//        public int getColor(int id) throws NotFoundException {
//            Log.e(TAG, "getColor(depr): " + getResourceEntryName(id));
//            return getColor(id, null);
//        }
//
//        @Override
//        public int getColor(int id, Theme theme) throws NotFoundException {
//            Log.e(TAG, "getColor: " + getResourceEntryName(id));
//
////            int resolvedResourceID = ThemeSupport.resolveToCustomThemeColorID(this, id, themeActivity.getLocalThemeName()); //0 is illigal (see super.getColor()), use it as indicator of no resolution
////            if (resolvedResourceID != 0) {
////                return ContextCompat.getColor(context, resolvedResourceID);
////            } else
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                return super.getColor(id, theme);
//            } else {
//                return super.getColor(id);
//            }
//        }
//
//        @Nullable
//        @Override
//        public ColorStateList getColorStateList(int id) throws NotFoundException {
//            Log.e(TAG, "getColorStateList(depr): " + getResourceEntryName(id));
//            return super.getColorStateList(id);
//        }
//
//        @Nullable
//        @Override
//        public ColorStateList getColorStateList(int id, Theme theme) throws NotFoundException {
//            Log.e(TAG, "getColorStateList(): " + getResourceEntryName(id));
//
//            ColorStateList selector = super.getColorStateList(id, theme);
//
////            int[][] states = new int[][]{
////                    new int[]{android.R.attr.state_pressed},  // pressed
////                    new int[]{} // default
////            };
////
////            int[] colors = new int[]{
////                    selector.getColorForState(states[1], selector.getDefaultColor()),
////                    selector.getColorForState(states[0], selector.getDefaultColor()),
////            };
////
////            selector = new ColorStateList(states, colors);
//            return selector;
//        }
//
//        @Override
//        public Drawable getDrawable(int id) throws NotFoundException {
//            Log.e(TAG, "getDrawable(depr): " + getResourceEntryName(id));
//            return getDrawable(id, null);
//        }
//
//        @Override
//        public Drawable getDrawable(int id, Theme theme) throws NotFoundException {
//            Log.e(TAG, "getDrawable(id, theme): " + getResourceEntryName(id));
//
////            if (id == R.drawable.shape_border_pressed) {
////                GradientDrawable dr = (GradientDrawable) ReusableFunctions.getTintedDrawable(context, id, R.color.selector_remove_team);
////                return dr;
////            } else
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                return super.getDrawable(id, theme);
//            } else {
//                return super.getDrawable(id);
//            }
//        }
//
//        @Override
//        public TypedArray obtainAttributes(AttributeSet set, int[] attrs) {
//            Log.e(TAG, "ATTRIBUTES");
//            TypedArray result = super.obtainAttributes(set, attrs);
//            for (int i = 0; i < result.getIndexCount(); i++) {
//                int id = result.getResourceId(i, 0);
//                if (id != 0) {
//                    Log.e(TAG, " getResourceEntryName: " + getResourceEntryName(id));
//                }
//            }
//            return result;
//        }
//
//        @Override
//        public void getValue(int id, TypedValue outValue, boolean resolveRefs) throws NotFoundException {
//            Log.e(TAG, "getValue() " + getResourceEntryName(id));
//            super.getValue(id, outValue, resolveRefs);
////
////            int resolvedID = ThemeSupport.resolveToCustomThemeColorID(this, id, themeActivity.getLocalThemeName());
////            if (resolvedID != 0) {
////                getValue(resolvedID, outValue, resolveRefs);
////            }
//        }
//
////        @Override
////        public void getValueForDensity(int id, int density, TypedValue outValue, boolean resolveRefs)
////                throws NotFoundException {
////            Log.e("getValue(density)", getResourceEntryName(id));
////            super.getValueForDensity(id, density, outValue, resolveRefs);
////        }
////
////        @Override
////        public void getValue(String name, TypedValue outValue, boolean resolveRefs)
////                throws NotFoundException {
////            Log.e("getValue(name): ", name);
////            super.getValue(name, outValue, resolveRefs);
////        }
//    }
}