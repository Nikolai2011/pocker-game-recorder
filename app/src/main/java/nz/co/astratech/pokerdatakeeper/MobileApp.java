package nz.co.astratech.pokerdatakeeper;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import nz.co.astratech.pokerdatakeeper.common.ThemeSupport;

public class MobileApp extends Application implements ThemeSupport.IActivityTheme{
    private static Context staticContext;

    @Override
    public void onCreate() {
        super.onCreate();

        staticContext = getApplicationContext();
    }

    public static Context getStaticContext() {
        return staticContext;
    }

    @Override
    public String getLocalThemeName() {
        return ThemeSupport.getCurrentGlobalTheme();
    }

    @Override
    public void setLocalThemeName(String theme) {
        return;
    }
}
