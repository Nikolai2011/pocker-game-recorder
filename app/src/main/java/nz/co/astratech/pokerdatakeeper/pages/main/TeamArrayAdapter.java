package nz.co.astratech.pokerdatakeeper.pages.main;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;

public class TeamArrayAdapter extends ArrayAdapter<TeamCredentialsModel> {
    public interface OnRemoveTeamClickedListener {
        void onRemoveTeamClicked(View v);
    }

    private Context context;
    private List<TeamCredentialsModel> arrayRecentlyAddedTeams; //Correctly resolve animation when views are recycled
    private OnRemoveTeamClickedListener listenerRemoveTeam;
    private List<TeamCredentialsModel> arrayTeams;
    private Comparator<TeamCredentialsModel> sortingRules;

    private View.OnClickListener removeTeamOnClickHandler;

    public TeamArrayAdapter(Context context, ArrayList<TeamCredentialsModel> teams) {
        super(context, 0, teams);
        this.context = context;

        arrayRecentlyAddedTeams = new ArrayList<>();
        removeTeamOnClickHandler = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listenerRemoveTeam != null) {
                    listenerRemoveTeam.onRemoveTeamClicked(v);
                }
            }
        };


        arrayTeams = teams;

        sortingRules = new Comparator<TeamCredentialsModel>() {
            @Override
            public int compare(TeamCredentialsModel o1, TeamCredentialsModel o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        Collections.sort(arrayTeams, sortingRules);
    }

    @Override
    public void add(TeamCredentialsModel object) {
        super.add(object);
        if (!arrayRecentlyAddedTeams.contains(object)) {
            arrayRecentlyAddedTeams.add(object);
        }
    }

    @Override
    public void notifyDataSetChanged() {
        Collections.sort(arrayTeams, sortingRules);
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TeamCredentialsModel team = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_local_team, parent, false);
        }

        ImageView btRemoveTeam = (ImageView)convertView.findViewById(R.id.image_button_remove_team);
        ReusableFunctions.setTintedImageDrawable(context, btRemoveTeam, R.drawable.svg_cross_circled, R.color.selector_remove_team);
        ReusableFunctions.setTintOnBackground(context, btRemoveTeam, R.color.selector_pressable_view_tint);
        btRemoveTeam.setTag(R.id.online_team, team);
        btRemoveTeam.setOnClickListener(removeTeamOnClickHandler);
        if (arrayRecentlyAddedTeams.contains(team)) {
            arrayRecentlyAddedTeams.remove(team);
            onAttachedAnimation(btRemoveTeam);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.textview_name);
        TextView tvID = (TextView) convertView.findViewById(R.id.textview_id);

        if (team == null) {
            tvName.setText(getContext().getString(R.string.unresolved_team));
        } else {
            String name = team.getName();
            if (name != null) {
                tvName.setText(name);
            }
            tvID.setText(String.format(getContext().getString(R.string.team_id_entry), team.getId()));
        }

        return convertView;
    }

    public void onAttachedAnimation(View btRemove) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(btRemove, "rotation", 0f, 720f);
        animator.setDuration(2000);
        animator.setRepeatCount(0);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.start();
    }

    public void setRemoveTeamClickedListener(OnRemoveTeamClickedListener listener) {
        this.listenerRemoveTeam = listener;
    }
}

