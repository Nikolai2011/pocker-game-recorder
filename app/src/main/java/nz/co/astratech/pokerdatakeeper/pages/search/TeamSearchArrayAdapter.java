package nz.co.astratech.pokerdatakeeper.pages.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;

public class TeamSearchArrayAdapter extends ArrayAdapter<TeamCredentialsModel> {
    public interface OnAddTeamClickedListener {
        void onAddTeamClicked(View v);
    }

    private Context context;
    private OnAddTeamClickedListener listenerAddTeam;
    private View.OnClickListener add_team_click_handler;

    public TeamSearchArrayAdapter(Context context, List<TeamCredentialsModel> teams) {
        super(context, 0, teams);
        this.context = context;

        add_team_click_handler = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listenerAddTeam != null) {
                    listenerAddTeam.onAddTeamClicked(v);
                }
            }
        };
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TeamCredentialsModel team = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_online_team, parent, false);
        }

        ImageView btAddTeam = (ImageView) convertView.findViewById(R.id.image_button_add_team);
        ReusableFunctions.setTintedImageDrawable(context, btAddTeam, R.drawable.svg_plus_circled, R.color.selector_add_team_tint);

        btAddTeam.setOnClickListener(add_team_click_handler);
        btAddTeam.setTag(R.id.online_team, team);

        TextView tvName = (TextView) convertView.findViewById(R.id.textview_name);
        TextView tvID = (TextView) convertView.findViewById(R.id.textview_id);

//        btAddTeam.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_DOWN){
//                    ((ImageButton)v).setColorFilter(Color.argb(255, 25, 25, 255));
//                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
//                    Rect bounds = new Rect(0, 0, v.getWidth(), v.getHeight());
//                    if (!bounds.contains((int)event.getX(), (int)event.getY())){
//                        ((ImageButton)v).setColorFilter(Color.argb(0, 0, 0, 0)); // No Tint
//                    }
//                }
//                return false;
//            }
//        });

        String name = team.getName();
        if (name != null) {
            tvName.setText(name);
        }
        tvID.setText(String.format(context.getString(R.string.team_id_entry), team.getId()));

        return convertView;
    }

    public void setListenerAddTeamClicked(OnAddTeamClickedListener listenerAddTeam) {
        this.listenerAddTeam = listenerAddTeam;
    }
}