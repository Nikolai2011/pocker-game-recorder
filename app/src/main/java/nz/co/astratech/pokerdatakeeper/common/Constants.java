package nz.co.astratech.pokerdatakeeper.common;

public class Constants {
    public static final int TEAM_NAME_MAX_ALLOWED_LENGHT = 50;

    //Data keys for bundles
    public static final String KEY_BUNDLE_TEAM_ID = "teamid";
    public static final String KEY_BUNDLE_TEAMNAME = "teamname";
    public static final String KEY_BUNDLE_PASSWORD = "password";
    public static final String KEY_BUNDLE_TEAM_MISSING = "team_missing";

    //Firebase Tree Nodes and Keys
    public static final String FIREBASE_NODE_TEAM_ID_NAME_MAPPING = "team_id_name_mapping";
    public static final String FIREBASE_NODE_TEAM_CREDENTIALS = "team_credentials";

    public static final String FIREBASE_KEY_TEAM_NAME = "name";
    public static final String FIREBASE_KEY_TEAM_PASSWORD = "password";
    public static final String FIREBASE_KEY_TEAM_PASS_CHECKING = "pass_checking";

    //INTENT REQUEST CODES
    public static final int INTENT_CREATE_LOGIN = 100;
    public static final int INTENT_ADD_TEAM = 101;
    public static final int INTENT_FIND_TEAM_ONLINE = 102;
    public static final int INTENT_PASSWORD_LOGIN = 103;
}
