package nz.co.astratech.pokerdatakeeper.pages.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;

import nz.co.astratech.pokerdatakeeper.common.AppData;
import nz.co.astratech.pokerdatakeeper.common.Constants;
import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;
import nz.co.astratech.pokerdatakeeper.pages.dialogs.DlgActivityPasswordLogin;
import nz.co.astratech.pokerdatakeeper.pages.search.ActivitySearchTeamOnline;
import nz.co.astratech.pokerdatakeeper.pages.dialogs.DlgActivityTeamCreation;
import nz.co.astratech.pokerdatakeeper.pages.team.ActivityTeamView;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;


public class MainActivity extends BaseActivity {
    private final String TAG = "MAIN ACTIVITY";

    //    private TeamAdapter adapterTeams;
    ArrayList<TeamCredentialsModel> arrayTeams;

    Button bt_create_team;
    Button bt_find_team_online;
    RecyclerView teamList;
    TeamAdapter teamAdapter;

    Toast toastFeedback;
    FrameLayout decorTeams;
    View teamListTouchEventCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toastFeedback = Toast.makeText(this, "", Toast.LENGTH_LONG);

        initUIFields();
        attachHandlers();

        ReusableFunctions.setTintedForeground(this, decorTeams, R.drawable.shape_border_decorator, R.color.selector_shape_border_decor_tint);

        //FOR REFERENCE
        //        dlg = new EasyDialog(this)
//                // .setLayoutResourceId(R.layout.layout_tip_content_horizontal)//layout resource id
//                .setLayout(llSearchTypes)
//                .setBackgroundColor(ContextCompat.getColor(this, R.color.gray))
//                //.setLocationByAttachedView(v)
//                //.setGravity(EasyDialog.GRAVITY_BOTTOM)
//                //.setAnimationTranslationShow(EasyDialog.DIRECTION_X, 1000, -600, 100, -50, 50, 0)
//                //.setAnimationAlphaShow(1000, 0.3f, 1.0f)
//                //.setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 500, -50, 800)
//                //.setAnimationAlphaDismiss(500, 1.0f, 0.0f)
//                .setTouchOutsideDismiss(true)
//                //.setMatchParent(true)
//        //.setMarginLeftAndRight(24, 24)
//        //.setOutsideColor(MainActivity.this.getResources().getColor(R.color.outside_color_trans))
//        ;
    }

    private void initUIFields() {
        bt_create_team = (Button) findViewById(R.id.bt_create_team);
        bt_find_team_online = (Button) findViewById(R.id.bt_find_team_online);

        teamList = (RecyclerView) findViewById(R.id.team_list);

        arrayTeams = new ArrayList<>();
        arrayTeams.addAll(AppData.share().getArrayTeamCredentials());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        teamList.setLayoutManager(layoutManager);
        teamAdapter = new TeamAdapter(arrayTeams);
        teamList.setAdapter(teamAdapter);
        decorTeams = (FrameLayout) findViewById(R.id.decor_teams);
        teamListTouchEventCatcher = findViewById(R.id.event_catcher);
    }

    private void attachHandlers() {
        bt_create_team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTeam();
            }
        });

        bt_find_team_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findTeamOnline();
            }
        });

        teamList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                decorTeams.onTouchEvent(event);
                return false;
            }
        });

        teamListTouchEventCatcher.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                decorTeams.onTouchEvent(event); //Updating border on press
                return false;
            }
        });

        teamAdapter.setOnElementClickListener(new TeamAdapter.OnElementClickListener() {
            @Override
            public void onElementClicked(View v) {
                TeamCredentialsModel team = (TeamCredentialsModel) v.getTag(R.id.online_team);

                if (team.isPasswordRequired()) {
                    loginIntoTeam(team.getName(), team.getId());
                } else {
                    viewTeamDetails(team.getId());
                }
            }

            @Override
            public void OnElementTouched(View v, MotionEvent event) {
                //decorTeams.onTouchEvent(event);
            }
        });

        teamAdapter.setOnRemoveTeamClickedListener(new TeamAdapter.OnRemoveTeamClickedListener() {
            @Override
            public void onRemoveTeamClicked(View v) {
                Object obj = v.getTag(R.id.online_team);
                if (obj instanceof TeamCredentialsModel) {
                    TeamCredentialsModel team = (TeamCredentialsModel) obj;
                    AppData.share().removeTeamCredentialsFromStorage(team.getId());

                    teamAdapter.removeTeam(team);

                    toastFeedback.setText(String.format(getString(R.string.team_removed), team.getName()));
                    toastFeedback.show();
                }
            }
        });

//        listViewTeams.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
//        listViewTeams.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
//            @Override
//            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
//                // Here you can do something when items are selected/de-selected,
//                // such as update the title in the CAB
//                listViewTeams.getChildAt(position).setSelected(checked);
//
//                CharSequence val = mode.getSubtitle();
//
//                if (val == null) {
//                    mode.setSubtitle("Item count: 1");
//                } else {
//                    String str = val.subSequence(12, mode.getSubtitle().length()).toString();
//                    int i = (checked) ? Integer.valueOf(str) + 1 : Integer.valueOf(str) - 1;
//                    mode.setSubtitle("Item count: " + String.valueOf(i));
//                }
//            }
//
//            @Override
//            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//                // Inflate the menu for the CAB
//                MenuInflater inflater = mode.getMenuInflater();
//                inflater.inflate(R.menu.menu_main, menu);
//                mode.setTitle("CAB Title");
//                return true;
//            }
//
//            @Override
//            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//                // Here you can perform updates to the CAB due to
//                // an invalidate() request
//                return false;
//            }
//
//            @Override
//            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//                // Respond to clicks on the actions in the CAB
//                switch (item.getItemId()) {
//                    case R.id.menu_create_team:
//                        return true;
//                    case R.id.menu_search_team:
//                        return true;
//                    default:
//                        return false;
//                }
//            }
//
//            @Override
//            public void onDestroyActionMode(ActionMode mode) {
//                // Here you can make any necessary updates to the activity when
//                // the CAB is removed. By default, selected items are deselected/unchecked.
//            }
//        });
    }

    @Override
    protected void onStart() {
        super.onStart();

//        if (arrayTeams.size() == 0) {
//            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    switch (which) {
//                        case DialogInterface.BUTTON_POSITIVE:
//                            createTeam();
//                            break;
//
//                        case DialogInterface.BUTTON_NEUTRAL:
//                            findTeamOnline();
//                            break;
//
//                        case DialogInterface.BUTTON_NEGATIVE:
//                            dialog.cancel();
//                            break;
//                    }
//                }
//            };

//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setMessage("There are currently no teams in the list. \n\nWhat would you like to do?")
//                    .setPositiveButton("Create a new team", dialogClickListener)
//                    .setNeutralButton("Find team online", dialogClickListener)
//                    .setNegativeButton("Cancel", dialogClickListener)
//                    .setTitle("Choose option")
//                    .setCancelable(false)
//                    .setIcon(android.R.drawable.ic_menu_share)
//                    .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && data.getExtras() != null) {
            Bundle bundle = data.getExtras();
            String teamID = bundle.getString(Constants.KEY_BUNDLE_TEAM_ID, null);
            String teamName = bundle.getString(Constants.KEY_BUNDLE_TEAMNAME, null);

            if (requestCode == Constants.INTENT_CREATE_LOGIN) {
                if (resultCode == RESULT_OK) {
                    String feedBackOnSuccess = String.format(getString(R.string.team_created), teamName);
                    addNewTeam(feedBackOnSuccess, teamID, teamName);
                }
            } else if (requestCode == Constants.INTENT_FIND_TEAM_ONLINE) {
                if (resultCode == RESULT_OK) {
                    String feedBackOnSuccess = String.format(getString(R.string.team_added), teamName);
                    addNewTeam(feedBackOnSuccess, teamID, teamName);
                }
            } else if (requestCode == Constants.INTENT_PASSWORD_LOGIN) {
                if (resultCode == RESULT_OK) {
                    viewTeamDetails(teamID);
                } else if (resultCode == RESULT_CANCELED) {
                    boolean teamIsMissing = data.getExtras().getBoolean(Constants.KEY_BUNDLE_TEAM_MISSING, false);
                    if (teamIsMissing) {
                        toastFeedback.setText(getString(R.string.team_does_not_exist));
                        toastFeedback.show();
                    }
                }
            }
        }
    }

    private void createTeam() {
//        DialogFragment dlgSignin = new DlgFragmentPasswordLogin();
//        dlgSignin.show(getFragmentManager(), "Team Creation");

        Intent intent = new Intent();
        intent.setClass(this, DlgActivityTeamCreation.class);
        startActivityForResult(intent, Constants.INTENT_CREATE_LOGIN);
    }

    private void findTeamOnline() {
        Intent intent = new Intent();
        intent.setClass(this, ActivitySearchTeamOnline.class);
        startActivityForResult(intent, Constants.INTENT_FIND_TEAM_ONLINE);
    }

    private void loginIntoTeam(String teamName, String teamID) {
        Intent intent = new Intent();
        intent.setClass(this, DlgActivityPasswordLogin.class);
        intent.putExtra(Constants.KEY_BUNDLE_TEAM_ID, teamID);
        intent.putExtra(Constants.KEY_BUNDLE_TEAMNAME, teamName);
        startActivityForResult(intent, Constants.INTENT_PASSWORD_LOGIN);
    }

    private void viewTeamDetails(String teamID) {
//        TeamCredentialsModel team = (TeamCredentialsModel) obj;

        Toast.makeText(this, " viewing details for " + teamID, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        intent.setClass(this, ActivityTeamView.class);
        startActivity(intent);
    }

    private void addNewTeam(String feedbackOnSuccess, String teamID, String teamName) {
        TeamCredentialsModel newTeam = new TeamCredentialsModel(teamID, teamName);

        if (arrayTeams.contains(newTeam)) {
            toastFeedback.setText(String.format(getString(R.string.team_already_in_the_list), teamName));
        } else {
            boolean teamAdded = AppData.share().addTeamCredentialsToStorage(newTeam);
            if (teamAdded) {
                toastFeedback.setText(feedbackOnSuccess);
                teamAdapter.addTeam(newTeam);
            } else {
                toastFeedback.setText(String.format(getString(R.string.error_creating_team), teamName));
            }
        }

        toastFeedback.show();
    }
}
