package nz.co.astratech.pokerdatakeeper.common;

import android.content.Context;
import android.util.Log;

import nz.co.astratech.pokerdatakeeper.R;

public final class TeamValidation {
    private final String TAG = "TeamValidation";
    private Context context;

    public TeamValidation(Context context) {
        this.context = context;
    }

    public String validateTeamID(String teamID, boolean checkEmpty) {
        String validationHint = null;

        if (teamID.matches(".*[$\\[\\]#/.].*")) {
            validationHint = context.getString(R.string.teamID_illigal_chars_hint);
        } else if (checkEmpty && teamID.isEmpty()) {
            validationHint = context.getString(R.string.teamID_missing_hint);
        }

        return validationHint;
    }

    public String validateTeamName(String teamname, boolean checkEmpty) {
        String validationHint = null;

        //1) For English Letters, digits, space.
        //teamname.matches(".*[^a-zA-Z0-9 ].*")

        //2) For everything except certain special characters.
        //teamname.matches(".*[!-/:-@\\[-^`{-~].*")

//        if (teamname.matches(".*[!-/:-@\\[-^`{-~].*")) {
//            validationHint = context.getString(R.string.teamname_illigal_chars_hint);
//        } else
        if (teamname.length() > Constants.TEAM_NAME_MAX_ALLOWED_LENGHT) {
            validationHint = String.format(context.getString(R.string.teamname_length_hint), Constants.TEAM_NAME_MAX_ALLOWED_LENGHT);
        } else if (checkEmpty && teamname.isEmpty()) {
            validationHint = context.getString(R.string.teamname_missing_hint);
        }

        return validationHint;
    }

    public String validatePassword(String password) {
        String validationHint = null;

        if (password.length() < 6) {
            validationHint = context.getString(R.string.password_length_hint);
        }

        return validationHint;
    }
}
