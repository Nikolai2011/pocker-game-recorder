package nz.co.astratech.pokerdatakeeper.pages.main;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;

import nz.co.astratech.pokerdatakeeper.common.ThemeSupport;

public class ThemeActivity extends AppCompatActivity implements ThemeSupport.IActivityTheme {
    private static final String STATE_ACTIVITY_THEME = "currentTheme";

    String currentActivityTheme = ThemeSupport.THEME_UNDEFINED;
    private ThemeSupport themeSupport;
    ThemeActivity self;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        themeSupport = new ThemeSupport(this);
        if (savedInstanceState != null) {
            currentActivityTheme = savedInstanceState.getString(STATE_ACTIVITY_THEME, ThemeSupport.THEME_UNDEFINED);
        }

        // MUST BE SET BEFORE setContentView
        themeSupport.updateActivityTheme();

        self = this;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!currentActivityTheme.equals(ThemeSupport.getCurrentGlobalTheme())) {
            // Reapply global theme if it has been changed.
            this.recreate();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_ACTIVITY_THEME, currentActivityTheme);
    }

    @Override
    public String getLocalThemeName() {
        return currentActivityTheme;
    }

    @Override
    public void setLocalThemeName(String theme) {
        currentActivityTheme = theme;
    }

    public ThemeSupport getThemeSupport() {
        if (themeSupport == null) {
            themeSupport = new ThemeSupport(this);
        }
        return themeSupport;
    }
}
