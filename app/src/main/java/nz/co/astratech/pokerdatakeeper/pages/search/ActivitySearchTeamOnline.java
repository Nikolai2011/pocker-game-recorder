package nz.co.astratech.pokerdatakeeper.pages.search;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import icepick.Icepick;
import icepick.State;
import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.common.Constants;
import nz.co.astratech.pokerdatakeeper.common.TeamValidation;
import nz.co.astratech.pokerdatakeeper.models.IceBundlerTeams;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;
import nz.co.astratech.pokerdatakeeper.pages.main.BaseActivity;

/**
 * Searching with both ID and Team Name fields (at the same time) seems nearly IMPOSSIBLE with Firebase API.
 */
public class ActivitySearchTeamOnline extends BaseActivity {
    private final String TAG = "ActivitySearchTeam";
    //    private static final String ARG_TEAMS = "DATA_TEAMS";
//    private static final String ARG_SEARCH_TYPE = "DATA_SEARCH_TYPE";

    private TeamSearchAdapter adapterTeamSearch;
    @State(IceBundlerTeams.class)
    protected ArrayList<TeamCredentialsModel> arrayOnlineTeams; //The elements of this array will be displayed on screen

    RecyclerView teamlist_search;
    EditText edittext_search_field;
    TextView textview_search_type;
    TextView textview_search;

    ImageView bt_search;
    Button bt_next;
    Button bt_prev;

    //Necessary junk?
    FrameLayout layoutSearchTypes; //Is the root of PopupWindow
    FrameLayout spinnerDecor; //The colored border of spinner
    ImageView spinnerOverlay; //Implements tinting on 2 images, on top of TextView with text.

    TeamValidation teamValidation;
    FirebaseTeamSearch teamSearch;
    GradientSearchAnimation borderGradient;
    Toast toastSearchFeedback;
    PopupWindow popupWindowSearchType;

    @State
    protected String valueToSearch = "";
    @State
    protected String savedTextViewSearchString = "";
    @State
    protected String savedTextViewSearchTypeString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_team);

        initUIFields();
        setupEventRouting();

        if (savedInstanceState != null) {
            Icepick.restoreInstanceState(this, savedInstanceState);

            textview_search.setText(savedTextViewSearchString);
            textview_search_type.setText(savedTextViewSearchTypeString);
        }

        teamValidation = new TeamValidation(this);
        if (arrayOnlineTeams == null) {
            arrayOnlineTeams = new ArrayList<>();
            //...else this will include search results stored by Icepick
        }
        adapterTeamSearch = new TeamSearchAdapter(arrayOnlineTeams);

        teamSearch = new FirebaseTeamSearch(this, textview_search_type.getText().toString());
        toastSearchFeedback = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        attachHandlers();

        //Recycler View Setup
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        teamlist_search.setLayoutManager(layoutManager);

        teamlist_search.setAdapter(adapterTeamSearch);

        borderGradient = new GradientSearchAnimation(this, new RectShape());
        ReusableFunctions.setBackground(teamlist_search, borderGradient);
        int pad = (int) borderGradient.getStrokeWidth();
        teamlist_search.setPadding(pad, pad, pad, pad);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

//        outState.putString(ARG_SEARCH_TYPE, teamSearch.getSearchType());
        savedTextViewSearchString = textview_search.getText().toString(); //Before Icepick
        savedTextViewSearchTypeString = textview_search_type.getText().toString();
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (arrayOnlineTeams.size() == 0) {
            bt_prev.setEnabled(false);
            bt_next.setEnabled(false);
        }
        //adapterTeamSearch.notifyDataSetChanged(); //Update in case we need to display previously saved search results
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        borderGradient.clearResources();
    }

    @Override
    protected String getActivityTitle() {
        return getString(R.string.title_activity_team_search);
    }

    private void initUIFields() {
        teamlist_search = (RecyclerView) findViewById(R.id.team_list);
        teamlist_search.post(new Runnable() {
            @Override
            public void run() {
                borderGradient.updateSize(teamlist_search.getWidth(), teamlist_search.getHeight());
                borderGradient.invalidateSelf();
            }
        });

        textview_search_type = (TextView) findViewById(R.id.textview_search_type);
        layoutSearchTypes = (FrameLayout) getLayoutInflater().inflate(R.layout.view_search_types, null);

        ReusableFunctions.setTintedForeground(this, layoutSearchTypes, R.drawable.shape_border_decorator, R.color.selector_shape_border_decor_tint);

        popupWindowSearchType = new PopupWindow(this);
        popupWindowSearchType.setBackgroundDrawable(new ColorDrawable(0)); //Remove border outline shadow
        popupWindowSearchType.setFocusable(true);
        popupWindowSearchType.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        popupWindowSearchType.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindowSearchType.setContentView(layoutSearchTypes);

        edittext_search_field = (EditText) findViewById(R.id.edittext_search_field);

        textview_search = (TextView) findViewById(R.id.textview_search_results);

        bt_search = (ImageView) findViewById(R.id.bt_search_team);
        bt_search.post(new Runnable() {
            @Override
            public void run() {
                //Width should be equal to height. Height depends on the layout calculations.
                ReusableFunctions.setSizeBasedOnHeight(bt_search, bt_search.getLayoutParams());
            }
        });

        bt_next = (Button) findViewById(R.id.bt_next);
        bt_prev = (Button) findViewById(R.id.bt_prev);
    }

    private void setupEventRouting() {
        //Setup search button with COLOR STATE LIST TINT DRAWABLE
        ReusableFunctions.setTintedImageDrawable(this, bt_search, R.drawable.png_search, R.color.selector_search_team_tint);

        //SETUP POPUP WINDOW INTERACTION WITHIN
        View.OnTouchListener searchTypeOnTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Let the root know (about the touch event) to apply border change color on press.
                v.onTouchEvent(event);
                layoutSearchTypes.setPressed(v.isPressed());
                return true; //Process touch event by itself
            }
        };
        layoutSearchTypes.findViewById(R.id.textview_search_by_name).setOnTouchListener(searchTypeOnTouchListener);
        layoutSearchTypes.findViewById(R.id.textview_search_by_id).setOnTouchListener(searchTypeOnTouchListener);

        //SETUP CUSTOM SPINNER INTERACTION
        spinnerDecor = (FrameLayout) findViewById(R.id.decor_spinner);
        ReusableFunctions.setTintedForeground(this, spinnerDecor, R.drawable.shape_border_decorator, R.color.selector_pressable_button_tint);

        spinnerOverlay = (ImageView) findViewById(R.id.imageview_spinner_overlay);
        ReusableFunctions.setTintedImageDrawable(this, spinnerOverlay, R.drawable.png_ninepatch_spinner_background, R.color.selector_pressable_button_tint);
        textview_search_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean result = v.onTouchEvent(event);
                spinnerOverlay.onTouchEvent(event);
                spinnerDecor.setPressed(v.isPressed());
                return result;
            }
        });
    }

    private void attachHandlers() {
        textview_search_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindowSearchType.setWidth(textview_search_type.getWidth());
                popupWindowSearchType.showAsDropDown(v, 0, 0);
            }
        });

        View.OnClickListener searchTypeOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutSearchTypes.setPressed(false);
                teamSearch.setSearchType(((TextView) v).getText().toString());
                popupWindowSearchType.dismiss();
            }
        };
        layoutSearchTypes.findViewById(R.id.textview_search_by_name).setOnClickListener(searchTypeOnClickListener);
        layoutSearchTypes.findViewById(R.id.textview_search_by_id).setOnClickListener(searchTypeOnClickListener);

        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSearch();
                onSearchClicked();
//                if (borderGradient.getIsSearching()) {
//                    borderGradient.setIsSearching(false);
//                    textview_search.setText("Not searching.");
//                } else {
//                    borderGradient.setIsSearching(true);
//                    textview_search.setText(getString(R.string.searching));
//                }
            }
        });
        bt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNextPage();
            }
        });

        bt_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPreviousPage();
            }
        });

        adapterTeamSearch.setListenerAddTeamClicked(new TeamSearchAdapter.OnAddTeamClickedListener() {
            @Override
            public void onAddTeamClicked(View v) {
                Object obj = v.getTag(R.id.online_team);
                if (obj instanceof TeamCredentialsModel) {
                    TeamCredentialsModel team = (TeamCredentialsModel) obj;

                    Intent intentResult = new Intent();

                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_BUNDLE_TEAM_ID, team.getId());
                    bundle.putString(Constants.KEY_BUNDLE_TEAMNAME, team.getName());
                    intentResult.putExtras(bundle);

                    setResult(RESULT_OK, intentResult);
                    finish();
                }
            }
        });

        teamSearch.setSearchTypeChangedListener(new FirebaseTeamSearch.SearchTypeChangedListener() {
            @Override
            public void onSearchTypeChanged(String searchType) {
                String hint = (teamSearch.isSearchByID()) ? getString(R.string.team_id) : getString(R.string.team_name);
                edittext_search_field.setHint(hint);

                if (!textview_search_type.getText().toString().equals(searchType)){
                    textview_search_type.setText(searchType);
                    resetSearch();
                    edittext_search_field.setText(""); //Empty search parameters

                    //Else search data should be preserved due to activity recreation...
                }
            }
        });

        teamSearch.setSearchStateChangedListener(new FirebaseTeamSearch.SearchStateChangedListener() {
            @Override
            public void onSearchStateChanged(boolean isSearching) {
                bt_search.setEnabled(!isSearching);
                if (isSearching) {
                    bt_next.setEnabled(false);
                    bt_prev.setEnabled(false);
                    textview_search.setText(getString(R.string.searching));
                }

                borderGradient.setIsSearching(isSearching);
            }
        });

        teamSearch.setSearchResultsArrivedListener(new FirebaseTeamSearch.SearchResultsArrivedListener() {
            @Override
            public void onSearchResultsArrived(List<TeamCredentialsModel> searchResults, boolean fromNextPage) {
                bt_next.setEnabled(true);
                bt_prev.setEnabled(true);
                adjustSearchResults(searchResults, fromNextPage);

                boolean noTeamFound = false;

                if (searchResults.size() == 0) {
                    String feedback;
                    if (arrayOnlineTeams.size() == 0) {
                        noTeamFound = true;
                        feedback = getString(R.string.no_matching_team_found);
                        textview_search.setText(String.format(getString(R.string.no_team_matching), "\"" + valueToSearch + "\":"));
                        bt_next.setEnabled(false);
                        bt_prev.setEnabled(false);
                    } else {
                        feedback = (fromNextPage) ? getString(R.string.end_of_data_reached) : getString(R.string.start_of_data_reached);
                        bt_next.setEnabled(!fromNextPage);
                        bt_prev.setEnabled(fromNextPage);
                    }

                    toastSearchFeedback.setText(feedback);
                    toastSearchFeedback.show();
                } else {
                    if (fromNextPage) {
                        arrayOnlineTeams.clear();
                        //arrayOnlineTeams.subList(0, elementsToRemove).clear();
                        arrayOnlineTeams.addAll(searchResults);
                    } else {
                        //SearchResults size should be no more than QUERY_ITEMS_LIMIT
                        //Therefore, elementsToRemove should be no more than we can actually remove from arrayOnlineTeams
                        int elementsToRemove = searchResults.size() + arrayOnlineTeams.size() - teamSearch.QUERY_ITEMS_LIMIT;
                        if (elementsToRemove > 0) {
                            arrayOnlineTeams.subList(arrayOnlineTeams.size() - elementsToRemove, arrayOnlineTeams.size()).clear(); //Keep some results from previous search for convenience
                        }
                        arrayOnlineTeams.addAll(0, searchResults);
                    }
                    adapterTeamSearch.notifyDataSetChanged();
                }

                if (!noTeamFound) {
                    textview_search.setText(String.format(getString(R.string.search_results_for), "\"" + valueToSearch + "\""));
                }
            }
        });
    }

    private void adjustSearchResults(List<TeamCredentialsModel> searchResults, boolean fromNextPage) {
        if (searchResults.size() == 0 || arrayOnlineTeams.size() == 0) {
            return;
        }

        if (fromNextPage && arrayOnlineTeams.get(arrayOnlineTeams.size() - 1).equals(searchResults.get(0))) {
            if (searchResults.size() == 1) {
                searchResults.remove(0);
            }
        } else if (!fromNextPage && arrayOnlineTeams.get(0).equals(searchResults.get(searchResults.size() - 1))) {
            searchResults.remove(searchResults.size() - 1);
        }
    }

    private void onSearchClicked() {
        hideSoftKeyboard();

        if (!ReusableFunctions.isConnectedToNetwork(this)) {
            toastSearchFeedback.setText(getString(R.string.no_network_access));
            toastSearchFeedback.show();
            return;
        }

        bt_search.setEnabled(false);

        if (teamSearch.isSearchByID()) {
            valueToSearch = edittext_search_field.getText().toString().trim();
        } else if (teamSearch.isSearchByName()) {
            valueToSearch = edittext_search_field.getText().toString();
        }

        if (!validateData()) {
            bt_search.setEnabled(true);
            return;
        }

        borderGradient.setIsMoveClockWise(true);
        teamSearch.postQuery(true, valueToSearch, teamSearch.convertToEndValue(valueToSearch));
    }

    private boolean validateData() {
        boolean isValid = true;

        String validationHint = null;

        if (teamSearch.isSearchByID()) {
            validationHint = teamValidation.validateTeamID(valueToSearch, false);
        } else if (teamSearch.isSearchByName()) {
            validationHint = teamValidation.validateTeamName(valueToSearch, false);
        }

        if (validationHint != null) {
            isValid = false;
            edittext_search_field.setError(validationHint);
        }

        return isValid;
    }

    private void loadNextPage() {
        try {
            if (arrayOnlineTeams.size() == 0) {
                bt_next.setEnabled(false);
                bt_prev.setEnabled(false);
                return;
            }

            borderGradient.setIsMoveClockWise(true);
            TeamCredentialsModel listEndObj = arrayOnlineTeams.get(arrayOnlineTeams.size() - 1);
            if (teamSearch.isSearchByID()) {
                teamSearch.postQuery(true, teamSearch.getSearchValueFromObj(listEndObj), teamSearch.convertToEndValue(valueToSearch));
            } else {
                //Search by name
                String startID = listEndObj.getId();
                String startName = listEndObj.getName();
                String endName = teamSearch.convertToEndValue(valueToSearch);
                teamSearch.postQuery(true, startID, null, startName, endName);
            }
        } catch (Exception ex) {
            Log.e(TAG, "loadNextPage failed: ", ex);
            toastSearchFeedback.setText(getString(R.string.error_loading_page));
            toastSearchFeedback.show();
        }
    }

    private void loadPreviousPage() {
        try {
            if (arrayOnlineTeams.size() == 0) {
                bt_next.setEnabled(false);
                bt_prev.setEnabled(false);
                return;
            }

            borderGradient.setIsMoveClockWise(false);
            TeamCredentialsModel listStartObj = arrayOnlineTeams.get(0);
            if (teamSearch.isSearchByID()) {
                teamSearch.postQuery(false, valueToSearch, teamSearch.getSearchValueFromObj(listStartObj));
            } else {
                String endID = listStartObj.getId();
                String startName = valueToSearch;
                String endName = listStartObj.getName();

                teamSearch.postQuery(false, null, endID, startName, endName);
            }
        } catch (Exception ex) {
            Log.e(TAG, "loadPreviousPage failed: ", ex);
            toastSearchFeedback.setText(getString(R.string.error_loading_page));
            toastSearchFeedback.show();
        }
    }

    private void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void resetSearch() {
        bt_next.setEnabled(false);
        bt_prev.setEnabled(false);
        textview_search.setText(getString(R.string.search_results));

        arrayOnlineTeams.clear();
        adapterTeamSearch.notifyDataSetChanged();
    }
}
