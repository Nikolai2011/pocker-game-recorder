package nz.co.astratech.pokerdatakeeper.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.annotations.NonNull;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import nz.co.astratech.pokerdatakeeper.MobileApp;
import nz.co.astratech.pokerdatakeeper.models.TeamCredentialsModel;

public class AppData {
    private static final String TAG = "AppData";

    //LOCAL STOREAGE KEYS
     private static final String KEY_TEAM_NAMES = "TEAM_NAMES";

    private static AppData appData = null;
    private Context curContext;

    private ArrayList<TeamCredentialsModel> arrayTeamCredentials;

    public static void reset() {
        appData = null;
    }

    public static AppData share() {
        if (appData == null) {
            appData = new AppData(MobileApp.getStaticContext());

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(appData.curContext);

            Type dataType = new TypeToken<List<TeamCredentialsModel>>() {}.getType();
            appData.arrayTeamCredentials = RITJsonHelper.toObject(sharedPref.getString(KEY_TEAM_NAMES, null), dataType);
            if (appData.arrayTeamCredentials == null) {
                appData.arrayTeamCredentials = new ArrayList<>();
            }
        }

        return appData;
    }

    private AppData(Context context) {
        this.curContext = context;
    }

    public boolean addTeamCredentialsToStorage(@android.support.annotation.NonNull TeamCredentialsModel newTeam) {
        if (arrayTeamCredentials == null || newTeam == null || newTeam.getName() == null || newTeam.getName().isEmpty()) {
            Log.e(TAG, "Invalid state: May be trying to save invalid (empty) team name/ID to local storage (or to null object ref).");
            return false;
        }

        if (!arrayTeamCredentials.contains(newTeam)) {
            arrayTeamCredentials.add(newTeam);
            rewriteArrayOfTeams();
        }

        return true;
    }

    public void removeTeamCredentialsFromStorage(String teamID) {
//        if (arrayTeamCredentials == null || teamID == null) {
//            Log.e(TAG, "Invalid state: Null object while saving team name to storage.");
//            return;
//        }

        arrayTeamCredentials.remove(new TeamCredentialsModel(teamID, null));
        rewriteArrayOfTeams();
    }


    public void allowAccessWithoutPassword(String teamID) {
        TeamCredentialsModel team = arrayTeamCredentials.get(arrayTeamCredentials.indexOf(new TeamCredentialsModel(teamID, null)));
        team.setPasswordRequired(false);
        rewriteArrayOfTeams();
    }

    private void rewriteArrayOfTeams() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(curContext);
        sharedPref.edit().putString(KEY_TEAM_NAMES, RITJsonHelper.toString(this.arrayTeamCredentials, false)).apply();
    }

    public ArrayList<TeamCredentialsModel> getArrayTeamCredentials() {
        return arrayTeamCredentials;
    }
}
