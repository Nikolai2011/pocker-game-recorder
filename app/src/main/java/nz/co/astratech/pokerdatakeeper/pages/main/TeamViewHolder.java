package nz.co.astratech.pokerdatakeeper.pages.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import nz.co.astratech.pokerdatakeeper.R;

public class TeamViewHolder extends RecyclerView.ViewHolder {
    Context context;

    LinearLayout elementWrapper;
    LinearLayout layoutLeft;
    ImageView btRemoveTeam;
    TextView tvName;
    TextView tvID;

    public TeamViewHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();

        elementWrapper = (LinearLayout) itemView.findViewById(R.id.element_wrapper);
        layoutLeft = (LinearLayout) itemView.findViewById(R.id.layout_left);
        btRemoveTeam = (ImageView) itemView.findViewById(R.id.image_button_remove_team);
        tvName = (TextView) itemView.findViewById(R.id.textview_name);
        tvID = (TextView) itemView.findViewById(R.id.textview_id);
    }
}
