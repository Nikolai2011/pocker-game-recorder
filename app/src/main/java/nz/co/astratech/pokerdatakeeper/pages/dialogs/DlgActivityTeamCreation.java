package nz.co.astratech.pokerdatakeeper.pages.dialogs;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import nz.co.astratech.pokerdatakeeper.common.Constants;
import nz.co.astratech.pokerdatakeeper.common.PasswordHashing;
import nz.co.astratech.pokerdatakeeper.common.TeamValidation;
import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;
import nz.co.astratech.pokerdatakeeper.pages.main.BaseActivity;

public class DlgActivityTeamCreation extends BaseActivity {
    private final String TAG = "DlgActivityPasswordLogin";

    DatabaseReference firebaseDBRoot;
    TeamValidation teamValidation;

    Button buttonOK;
    Button buttonCancel;
    TextView textView_hint;
    EditText editText_teamname;
    EditText editText_password;
    Toast toastNoConnection;

    private String keyTeamID;
    private String selectedTeamname;
    private String selectedPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);//Before SetContentView()
        setContentView(R.layout.activity_create_team);

        //Useful with some themes
        //getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        firebaseDBRoot = FirebaseDatabase.getInstance().getReference();
        teamValidation = new TeamValidation(this);
        toastNoConnection = Toast.makeText(this, getString(R.string.no_network_access), Toast.LENGTH_SHORT);

        buttonOK = (Button) findViewById(R.id.bt_ok);
        buttonCancel = (Button) findViewById(R.id.bt_cancel);
        textView_hint = (TextView) findViewById(R.id.textview_hint);

        editText_teamname = (EditText) findViewById(R.id.edittext_team_name);
        editText_password = (EditText) findViewById(R.id.edittext_password);

        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOKClicked();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancelClicked();
            }
        });
    }

    private void handleOKClicked() {
        if (!ReusableFunctions.isConnectedToNetwork(this)) {
            showFeedback(getString(R.string.no_network_access));
            toastNoConnection.setText(getString(R.string.no_network_access));
            toastNoConnection.show();
            return;
        }

        buttonOK.setEnabled(false);
        selectedTeamname = editText_teamname.getText().toString().trim();
        selectedPassword = editText_password.getText().toString();

        if (!validateData()) {
            buttonOK.setEnabled(true);
            return;
        }

        String passwordHash = PasswordHashing.hashFromPassword(selectedPassword);

        try {
            keyTeamID = firebaseDBRoot.child(Constants.FIREBASE_NODE_TEAM_CREDENTIALS).push().getKey();

            Map<String, Object> teamCredentialsPost = new HashMap<>();
            teamCredentialsPost.put(Constants.FIREBASE_KEY_TEAM_NAME, selectedTeamname);
            teamCredentialsPost.put(Constants.FIREBASE_KEY_TEAM_PASSWORD, passwordHash);

            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put(Constants.FIREBASE_NODE_TEAM_CREDENTIALS + "/" + keyTeamID, teamCredentialsPost);
            childUpdates.put(Constants.FIREBASE_NODE_TEAM_ID_NAME_MAPPING + "/" + keyTeamID, selectedTeamname);

            firebaseDBRoot.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError == null) {
                        Intent intentResult = new Intent();

                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.KEY_BUNDLE_TEAM_ID, keyTeamID);
                        bundle.putString(Constants.KEY_BUNDLE_TEAMNAME, selectedTeamname);
                        intentResult.putExtras(bundle);

                        setResult(RESULT_OK, intentResult);
                        finish();
                    } else {
                        textView_hint.setText(String.format(getString(R.string.error), ": " + databaseError.getMessage()));
                    }

                    buttonOK.setEnabled(true);
                }
            });

//            firebaseDBRoot.child(Constants.FIREBASE_TEAM_IDS + "/" + selectedTeamname).setValue(selectedPassword, new DatabaseReference.CompletionListener()

        } catch (DatabaseException ex) {
            textView_hint.setText(String.format(getString(R.string.error), ": " + ex.getMessage()));
            buttonOK.setEnabled(true);
        }
    }

    private void handleCancelClicked() {
        Intent intentResult = new Intent();
        setResult(RESULT_CANCELED, intentResult);

        finish();
    }

    private boolean validateData() {
        boolean isValid = true;

        String validationHint = null;
        String passwordHint = teamValidation.validatePassword(selectedPassword);
        String teamNameHint = teamValidation.validateTeamName(selectedTeamname, true);

        if (teamNameHint != null) {
            validationHint = teamNameHint;
        } else if (passwordHint != null) {
            validationHint = passwordHint;
        }

        if (validationHint != null) {
            isValid = false;
            showFeedback(validationHint);
        }

        return isValid;
    }

    private void showFeedback(String feedback) {
        if (textView_hint.getText().toString().equals(feedback)) {
            textView_hint.setTypeface(null, Typeface.BOLD);
        } else {
            textView_hint.setTypeface(null, Typeface.NORMAL);
        }

        textView_hint.setText(feedback);
    }
}
