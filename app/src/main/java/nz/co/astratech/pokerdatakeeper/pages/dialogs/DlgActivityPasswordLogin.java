package nz.co.astratech.pokerdatakeeper.pages.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import nz.co.astratech.pokerdatakeeper.R;
import nz.co.astratech.pokerdatakeeper.common.AppData;
import nz.co.astratech.pokerdatakeeper.common.Constants;
import nz.co.astratech.pokerdatakeeper.common.PasswordHashing;
import nz.co.astratech.pokerdatakeeper.reusable.ReusableFunctions;
import nz.co.astratech.pokerdatakeeper.pages.main.BaseActivity;

public class DlgActivityPasswordLogin extends BaseActivity {
    private final String TAG = "PasswordLogin";

    private String loginTeamID;
    private String loginTeamName;

    DatabaseReference firebaseDBRoot;
    DatabaseReference firebaseTeamsRef;
    DatabaseReference firebaseTeamsCredentialsRef;
    ValueEventListener teamRemovalListener;

    CheckBox checkBoxNotAskAgain;
    Button buttonOK;
    Button buttonCancel;
    EditText editText_password;
    TextView textviewHint;

    Toast toastFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //requestWindowFeature(Window.FEATURE_ACTION_BAR);//Before SetContentView()
        setContentView(R.layout.activity_dlg_password_login);

        try {
            loginTeamID = getIntent().getExtras().getString(Constants.KEY_BUNDLE_TEAM_ID);
            loginTeamName = getIntent().getExtras().getString(Constants.KEY_BUNDLE_TEAMNAME);
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, getString(R.string.error_detecting_team), Toast.LENGTH_SHORT).show();
            finish();
        }

        firebaseDBRoot = FirebaseDatabase.getInstance().getReference();
        firebaseTeamsRef = firebaseDBRoot.child(Constants.FIREBASE_NODE_TEAM_ID_NAME_MAPPING + "/" + loginTeamID);
        firebaseTeamsCredentialsRef = firebaseDBRoot.child(Constants.FIREBASE_NODE_TEAM_CREDENTIALS + "/" + loginTeamID + "/" + Constants.FIREBASE_KEY_TEAM_PASS_CHECKING);
        teamRemovalListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object obj = dataSnapshot.getValue();
                if (obj == null) {
                    Intent intentResult = new Intent();

                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.KEY_BUNDLE_TEAM_MISSING, true);
                    intentResult.putExtras(bundle);

                    setResult(RESULT_CANCELED, intentResult);
                    firebaseTeamsRef.removeEventListener(teamRemovalListener);
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "teamRemovalListener: onCancelled!\n" + databaseError.getMessage());
            }
        };

        toastFeedback = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        buttonOK = (Button) findViewById(R.id.bt_ok);
        buttonCancel = (Button) findViewById(R.id.bt_cancel);
        editText_password = (EditText) findViewById(R.id.edittext_password);
        checkBoxNotAskAgain = (CheckBox) findViewById(R.id.checkbox_not_ask_again);
        textviewHint = (TextView) findViewById(R.id.textview_hint);
        textviewHint.setText(getString(R.string.team) + ": " + loginTeamName);

        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOKClicked();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleCancelClicked();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        firebaseTeamsRef.addValueEventListener(teamRemovalListener);
    }

    @Override
    protected void onStop() {
        super.onStop();

        firebaseTeamsRef.removeEventListener(teamRemovalListener);
    }

    private void handleOKClicked() {
        if (!ReusableFunctions.isConnectedToNetwork(this)) {
            toastFeedback.setText(getString(R.string.no_network_access));
            toastFeedback.show();
            return;
        }

        buttonOK.setEnabled(false);
        String password = editText_password.getText().toString();
        String passwordHash = PasswordHashing.hashFromPassword(password);

        firebaseTeamsCredentialsRef.setValue(passwordHash, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    if (checkBoxNotAskAgain.isChecked()) {
                        AppData.share().allowAccessWithoutPassword(loginTeamID);
                    }
                    Intent intentResult = new Intent();

                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.KEY_BUNDLE_TEAM_ID, loginTeamID);
                    intentResult.putExtras(bundle);

                    setResult(RESULT_OK, intentResult);
                    finish();
                } else if (databaseError.getCode() == DatabaseError.PERMISSION_DENIED) {
                    toastFeedback.setText(getString(R.string.wrong_password));
                    toastFeedback.show();
                } else {
                    toastFeedback.setText("Unknown error. You could try again later.");
                    toastFeedback.show();
                }

                buttonOK.setEnabled(true);
            }
        });
    }

    private void handleCancelClicked() {
        Intent intentResult = new Intent();
        setResult(RESULT_CANCELED, intentResult);

        finish();
    }
}
