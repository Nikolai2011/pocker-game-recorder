package nz.co.astratech.pokerdatakeeper.models;

import android.os.Bundle;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import icepick.Bundler;
import nz.co.astratech.pokerdatakeeper.common.RITJsonHelper;

public class IceBundlerTeams implements Bundler<List<TeamCredentialsModel>> {
    public void put(String key, List<TeamCredentialsModel> value, Bundle bundle) {
        bundle.putString(key, RITJsonHelper.toString(value, false));
    }

    public List<TeamCredentialsModel> get(String key, Bundle bundle) {
        Type dataType = new TypeToken<List<TeamCredentialsModel>>() {}.getType();
        return RITJsonHelper.toObject(bundle.getString(key, null), dataType);
    }
}
