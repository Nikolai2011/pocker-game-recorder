package nz.co.astratech.pokerdatakeeper.pages.search;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.util.DisplayMetrics;

public class GradientSearchAnimation extends ShapeDrawable {
    private final GradientSearchAnimation self = this;
    private final int CONST_GRADIENT_COLOR = Color.BLUE;
    private final int DYNAMIC_GRADIENT_COLOR_REST = Color.GREEN;
    private final int DYNAMIC_GRADIENT_COLOR_SEARCH = Color.RED;
    private final int ANIMATION_DURATION_MILLI = 3000;
    private final float ANIM_MOVE_START_ANGLE = 0.0f;
    private final float ANIM_MOVE_END_ANGLE = 360.0f;

    private ActivitySearchTeamOnline activitySearch; //Have something update UI on main thread

    private Matrix gradientMatrix;
    private int width, height; //The size of view for which this gradient applies
    private Paint fillPaint, strokePaint;
    private Shader dynamicGradient;
    private ValueAnimator animMovement;
    private ValueAnimator animColor;

    private boolean isSearching = false;
    private boolean isMoveClockWise = true;
    private TimeInterpolator searchStartInterpolator;
    private TimeInterpolator searchContinuesInterpolator;
    private TimeInterpolator searchEndInterpolator;

    private int currentGradientColor = DYNAMIC_GRADIENT_COLOR_REST;
    private int[] gradientColors;
    private float[] gradientColorPositions = new float[]{0f, 0.25f, 0.5f, 0.75f, 1f};

    public GradientSearchAnimation(ActivitySearchTeamOnline activitySearch, Shape s) {
        super(s);
        if (activitySearch == null || s == null) {
            throw new NullPointerException();
        }

        initInterpolators();
        this.activitySearch = activitySearch;
        DisplayMetrics metrics = activitySearch.getResources().getDisplayMetrics();
        int dpi = metrics.densityDpi;

        fillPaint = this.getPaint();
        strokePaint = new Paint(fillPaint);
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setStrokeWidth(10 * dpi / 96.0f);

        gradientMatrix = new Matrix();
        initAnimation();
        attachAnimHandlers();

        updateGradientColor();
        updateSize(100, 100);
    }

    private void initAnimation() {
        animMovement = (isMoveClockWise) ? ValueAnimator.ofFloat(ANIM_MOVE_START_ANGLE, ANIM_MOVE_END_ANGLE) : ValueAnimator.ofFloat(ANIM_MOVE_END_ANGLE, ANIM_MOVE_START_ANGLE);
        animMovement.setDuration(ANIMATION_DURATION_MILLI);
        animMovement.setRepeatCount(ValueAnimator.INFINITE);
        animMovement.setRepeatMode(ValueAnimator.RESTART);

        animColor = ValueAnimator.ofObject(new ArgbEvaluator(), DYNAMIC_GRADIENT_COLOR_REST, DYNAMIC_GRADIENT_COLOR_SEARCH);
        animColor.setDuration(ANIMATION_DURATION_MILLI);
        animColor.setRepeatCount(ValueAnimator.INFINITE);
        animColor.setRepeatMode(ValueAnimator.RESTART);
    }

    public void attachAnimHandlers() {
        animMovement.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                try {
                    Float angle = (Float) animation.getAnimatedValue();
                    rotate(angle);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        animMovement.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationRepeat(Animator animation) {
                if (isSearching) {
                    if (animMovement.getInterpolator() == searchStartInterpolator) {
                        animMovement.setInterpolator(searchContinuesInterpolator);
                    } else if (animMovement.getInterpolator() == searchEndInterpolator) {
                        animMovement.setInterpolator(searchStartInterpolator);
                    }
                } else if (!isSearching) {
                    if (animMovement.getInterpolator() != searchEndInterpolator) {
                        animMovement.setInterpolator(searchEndInterpolator);
                    } else {
                        animMovement.end();
                    }
                }
            }
        });

        animColor.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                currentGradientColor = (int) animation.getAnimatedValue();
                updateGradientColor();
            }
        });
        animColor.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationRepeat(Animator animation) {
                if (animMovement.getInterpolator() == searchStartInterpolator ||
                        animMovement.getInterpolator() == searchContinuesInterpolator) {
                    animColor.end(); // Required for api 19
                    animColor.setObjectValues(currentGradientColor, DYNAMIC_GRADIENT_COLOR_SEARCH);
                    animColor.start(); // Required for api 19
                } else if (animMovement.getInterpolator() == searchEndInterpolator) {
                    animColor.end(); // Required for api 19
                    animColor.setObjectValues(currentGradientColor, DYNAMIC_GRADIENT_COLOR_REST);
                    animColor.start(); // Required for api 19
                }

                if (!animMovement.isRunning()) {
                    currentGradientColor = DYNAMIC_GRADIENT_COLOR_REST;
                    animColor.end();
                }
            }
        });
    }

    @Override
    protected void onDraw(Shape shape, Canvas canvas, Paint paint) {
        shape.draw(canvas, strokePaint);
    }

    private void rotate(float angle) {
        gradientMatrix.setRotate(angle, width / 2, height / 2);
        updateGradient();
        activitySearch.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                self.invalidateSelf();
            }
        });
    }

    public void updateSize(int width, int height) {
        this.width = width;
        this.height = height;

        float ratio = Math.max(width, height) / Math.min(width, height);
        if (ratio > 1.5) {
            float desiredDistance = Math.min(width, height) / 4.0f;
            float diagonal = (float) Math.sqrt(width * width + height * height);
            float offset = desiredDistance / diagonal;
            float leftBound = Math.max(0.0f, 0.5f - 2 * offset);
            float rightBound = Math.min(1.0f, 0.5f + 2 * offset);
            gradientColorPositions = new float[]{leftBound, 0.5f - offset, 0.5f, 0.5f + offset, rightBound};
        } else {
            gradientColorPositions = new float[]{0f, 0.25f, 0.5f, 0.75f, 1f};
        }

        //RAINBOW
//        float[] fractions = new float[7];
//        for (int i = 0; i < fractions.length; i++) {
//            fractions[i] = i / (float) fractions.length;
//        }
//        gradientColorPositions = fractions;

        updateGradient();
    }

    private void updateGradient() {
        dynamicGradient = new LinearGradient(0, 0, width, height,
                gradientColors, gradientColorPositions, Shader.TileMode.CLAMP);
        dynamicGradient.setLocalMatrix(gradientMatrix);
        strokePaint.setShader(dynamicGradient);
    }

    private void updateGradientColor() {
        //gradientColors = new int[]{Color.CYAN, Color.BLUE, Color.rgb(255, 0, 255), Color.RED, Color.rgb(200,100,0), Color.YELLOW, Color.GREEN};
        gradientColors = new int[]{CONST_GRADIENT_COLOR, currentGradientColor, CONST_GRADIENT_COLOR, currentGradientColor, CONST_GRADIENT_COLOR};

        if (!animMovement.isRunning()) {
            activitySearch.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    self.invalidateSelf();
                }
            });
        }
    }

    public void endAnimation() {
        animMovement.end();
        animColor.end();

        rotate(0);
        updateGradientColor();
    }

    public void clearResources() {
        animMovement.end();
        animMovement.removeAllListeners();
        animMovement.removeAllUpdateListeners();

        animColor.end();
        animColor.removeAllListeners();
        animColor.removeAllUpdateListeners();
    }

    public void setIsSearching(boolean isSearching) {
        if (this.isSearching == isSearching) {
            return;
        }

        this.isSearching = isSearching;

        if (!animMovement.isRunning() && isSearching) {
            if (isMoveClockWise) {
                animMovement.setFloatValues(ANIM_MOVE_START_ANGLE, ANIM_MOVE_END_ANGLE);
            } else {
                animMovement.setFloatValues(ANIM_MOVE_END_ANGLE, ANIM_MOVE_START_ANGLE);
            }
            animMovement.setInterpolator(searchStartInterpolator);
            animMovement.start();
        }

        if (!animColor.isRunning() && isSearching) {
            animColor.setObjectValues(DYNAMIC_GRADIENT_COLOR_REST, DYNAMIC_GRADIENT_COLOR_SEARCH);
            animColor.start();
        }
    }

    public boolean getIsSearching() {
        return isSearching;
    }

    private void initInterpolators() {
        searchStartInterpolator = new TimeInterpolator() {
            @Override
            public float getInterpolation(float input) {
                return funcParabola(input);
            }
        };

        searchContinuesInterpolator = new TimeInterpolator() {
            @Override
            public float getInterpolation(float input) {
                return funcTangentLineToParabolaEnd(input);
            }
        };

        searchEndInterpolator = new TimeInterpolator() {
            @Override
            public float getInterpolation(float input) {
                return funcParabolaReverse(input);
            }
        };
    }

    private float funcParabola(float x) {
        return 0.5f * x * x + 0.5f * x;

        //Slow acceleration at start, greater at the end.
//        return 0.8f * x * x + 0.2f * x;
    }

    private float funcParabolaReverse(float x) {
//        Reflexion over -x + 1
        return 1 - funcParabola(1 - x);
    }

    private float funcTangentLineToParabolaEnd(float x) {
        return 1.5f * x;
//        return 1.8f * x;
    }

    public float getStrokeWidth() {
        return strokePaint.getStrokeWidth();
    }

    public boolean isMoveClockWise() {
        return isMoveClockWise;
    }

    public void setIsMoveClockWise(boolean clockWise) {
        isMoveClockWise = clockWise;
    }
}
